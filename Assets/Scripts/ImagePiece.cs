﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagePiece : MonoBehaviour
{
    float localSpeed;
    float maxSpeed = 2f;
    Vector3 velocityVector;

    public void SetupMovement(float speed)
    {
        localSpeed = speed;
        if(localSpeed > maxSpeed)
        {
            print("Max speed allowed is " + maxSpeed.ToString()); //To avoid mistakes in movement behaviour
            localSpeed = maxSpeed;
        }

        velocityVector = new Vector3(0f, 0f, localSpeed);
    }

    private void FixedUpdate()
    {
        transform.localPosition += velocityVector;

        if (DetectIfDestinationReached())
            OnDestinationReached();
    }

    //TODO Fix detection so it works with varying end points, as needed for #ARmode
    private bool DetectIfDestinationReached()
    {
        bool isReached = false;
        if (transform.localPosition.z > 0f)
            isReached = true;

        return isReached;
    }

    private void OnDestinationReached()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -0.001f); //Snap to be on top of canvas
        Destroy(this); //Clean up
    }
}
