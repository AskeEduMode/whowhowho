﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideOnPlay : MonoBehaviour
{
    //Hide this in builds
#if !UNITY_EDITOR
    private void Start()
    {
        try
        {
            transform.GetComponent<Renderer>().enabled = false;
        }
        catch
        {
            Debug.LogWarning("Can't get renderer.");
        }
    }
#endif
}
