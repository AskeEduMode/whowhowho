﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ImageBuilderTextureCubes : MonoBehaviour
{
    //------    Properties

    //Canvas
    [SerializeField] Transform canvas;
    Vector3 canvasPosition;
    Renderer canvasRenderer;

    //Image Piece
    [SerializeField] GameObject piecePrefab;
    [SerializeField] float pieceSpeed; //#DevPurpose (not to be SerializeField in release)
    [SerializeField] float pieceSpeedVariation; //#DevPurpose (not to be SerializeField in release)

    //Positioning
    [SerializeField] bool isAR; //When #ARmode, it should vary the end positioning on z-axis of pieces
    [SerializeField] Transform pointForMaxEndDistance;
    float endDistanceVariation;
    [SerializeField] Transform pieceStartingDistancePoint;

    
    //Grid
    [SerializeField] List<Vector2> gridPositions; //#DevPurpose (not to be SerializeField in release)
    [SerializeField] int piecesPerSide; //#DevPurpose (not to be SerializeField in release)
    int currentPieceID = 0;
    float shift;

    
    //------    Unity methods
    private void Awake()
    {
        canvasRenderer = canvas.GetComponent<Renderer>();
    }

    //------ Public methods
    public void BuildImage()
    {
        //#ARmode
        if (isAR)
            endDistanceVariation = Vector3.Distance(canvas.position, pointForMaxEndDistance.position);
        else
            endDistanceVariation = 0f;

        SetRandomDimensions();
        BuildGrid();

        int piecesAmount = piecesPerSide * piecesPerSide;
        for (int i = 0; i < piecesAmount; i++)
            LaunchPiece();
    }

    //--- #DevPurpose (for easy restart)
    public void ResetScene()
    {
        SceneManager.LoadScene(0);
    }
//---

//------ Private methods
void SetRandomDimensions() //#DevPurpose, a temporary solution to simply display varying versions
    {
        piecesPerSide = Random.Range(2, 6);
    }

    private void LaunchPiece()
    {
        GameObject tempGameObject;
        try
        {
            tempGameObject = Instantiate<GameObject>(piecePrefab, canvas);
        }
        catch
        {
            Debug.LogWarning("Couldn't instantiate image piece.");
            return;
        }

        //Scaling piece, for grid to fill out canvas
        tempGameObject.transform.localScale = new Vector3(1f / piecesPerSide, 1f / piecesPerSide, 1f / piecesPerSide);

        DefineOriginAndDestination(tempGameObject);

        //Set speed of piece
        SetupPieceSpeed(tempGameObject);

        currentPieceID++;
    }

    private void DefineOriginAndDestination(GameObject pieceObject)
    {
        //Origin (setting z-axis in world space)
        pieceObject.transform.localPosition = new Vector3(gridPositions[currentPieceID].x, gridPositions[currentPieceID].y, 0f);

        //TODO Make sure to support #ARmode with varying end destination
        float endPosZ = pieceStartingDistancePoint.position.z + endDistanceVariation;
        pieceObject.transform.position = new Vector3(pieceObject.transform.position.x + shift, pieceObject.transform.position.y + shift,
            endPosZ);

        //Destination
        Vector3 tempDestination = new Vector3(pieceObject.transform.localPosition.x, pieceObject.transform.localPosition.y,
            canvas.transform.position.z - 0.01f);
    }

    private void SetupPieceSpeed(GameObject pieceObject)
    {
        ImagePiece tempImagePiece = pieceObject.GetComponent<ImagePiece>();

        float speed = pieceSpeed + Random.Range(-pieceSpeedVariation, pieceSpeedVariation);
        tempImagePiece.SetupMovement(speed);
    }

    private void BuildGrid()
    {
        gridPositions = new List<Vector2>();

        if(piecesPerSide == 0)
        {
            Debug.LogWarning("Need more than 0 pieces per side in order to not divide by 0.");
            return;
        }

        float pieceWidth = 1f / (float) piecesPerSide;
        UpdateShift(pieceWidth);

        for (int i = 0; i < piecesPerSide; i++)
        {
            for(int j = 0; j < piecesPerSide; j++)
            {
                gridPositions.Add(
                    new Vector2((i * pieceWidth), (j*pieceWidth))
                    );
            }
        }
    }

    private void UpdateShift(float pieceWidth)
    {
        float centerOnCorner = canvasRenderer.bounds.max.x;
        shift = centerOnCorner + (-0.5f * (pieceWidth * canvas.transform.localScale.x) );
        shift *= -1f;
    }

}