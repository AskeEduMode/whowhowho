﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickDelete : MonoBehaviour
{
   public void DeleteMe()
    {
        Destroy(gameObject);
    }
}
